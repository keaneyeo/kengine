#include "stdafx.h"

#include "KFile.h"


KFile::KFile(const char* pPath)
{
	START_REF();
	m_Name = pPath;
	m_hFile = INVALID_HANDLE_VALUE;
}


KFile::~KFile()
{
}

int KFile::Read(void * _buffer, long _size)
{
	DWORD bytesread;
	if (!::ReadFile(m_hFile, _buffer, _size, &bytesread, NULL))
		return -1;
	m_iPosition += bytesread;
	return (int)bytesread;
}

int KFile::Write(void * _buffer, long _size)
{
	DWORD byteswritten;
	if (!::WriteFile(m_hFile, _buffer, _size, &byteswritten, NULL))
	{
		return -1;
	}
	m_iPosition += byteswritten;
	return (int)byteswritten;
}

int KFile::Seek(long _offset, eStreamOrigin _origin)
{
	if (m_hFile == NULL)
		return -1;
	DWORD movemethod = 0;
	switch (_origin)
	{
	case foStart:
		movemethod = FILE_BEGIN;
		m_iPosition = 0; break;
	case foCurrent:
		movemethod = FILE_CURRENT; break;
	case foEnd:
		movemethod = FILE_END;
		m_iPosition = Length() - 1; break;
	default:
		return -1;
	}

	DWORD ret = SetFilePointer(m_hFile, _offset, NULL, movemethod);
	m_iPosition += _offset;
	return (INT)(ret);
}

bool KFile::Eof()
{
	if (m_hFile)
		return (Position() == Length()) ? TRUE : FALSE;

	return FALSE;
}

long KFile::Length()
{
	if (m_hFile)
		return GetFileSize(m_hFile, NULL);

	return -1;
}

long KFile::Position()
{
	if (m_hFile == NULL)
		return -1;

	return ::SetFilePointer(m_hFile, 0, NULL, FILE_CURRENT);
}

void * KFile::GetBuffer()
{
	return NULL;
}

const char * KFile::GetPath()
{
	return m_Name.c_str();
}

DWORD ParseAccessMode(eFileAccessMode _accessMode)
{
	DWORD accessMode = 0;
	if (_accessMode & famRead)
		accessMode |= GENERIC_READ;

	if (_accessMode & famWrite)
		accessMode |= GENERIC_WRITE;

	return accessMode;
}

DWORD ParseOpenMode(eFileAccessMode _accessMode)
{
	DWORD openmode = 0;
	if (_accessMode & famCreate)
		openmode |= CREATE_ALWAYS;
	else
		openmode |= OPEN_EXISTING;

	return openmode;
}

bool KFile::Open(eFileAccessMode _accessMode)
{
	DWORD access = ParseAccessMode(_accessMode);
	DWORD openMode = ParseOpenMode(_accessMode);
	DWORD flags = FILE_ATTRIBUTE_NORMAL;
	DWORD sharing = FILE_SHARE_READ;

	if (m_hFile == INVALID_HANDLE_VALUE)
	{
		m_hFile = ::CreateFileA(m_Name.c_str(), access,
			sharing, NULL, openMode, flags, NULL);
		return true;
	}
	else
		return false;
}

void KFile::Close()
{
	if (m_hFile)
		::CloseHandle(m_hFile);
	m_hFile = NULL;
}

void KFile::Flush()
{
	if (m_hFile != NULL)
		FlushFileBuffers(m_hFile);

	m_hFile = NULL;
}

void * KFile::GetHandle()
{
	return m_hFile;
}

iKFile *K_LoadFile(const char* _path, eFileAccessMode _accessMode)
{
	iKFile* file = new KFile(_path);
	if (file->Open(_accessMode))
		return file;
	else
	{
		delete file;
		file = NULL;
		return file;
	}
}
