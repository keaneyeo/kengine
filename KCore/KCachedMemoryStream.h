#pragma once
#include "iKStream.h"
class KCachedMemoryStream : public iKByteStream
{
public:
       KCachedMemoryStream(long size);
       KCachedMemoryStream(iKByteStream* file);
       virtual ~KCachedMemoryStream();
       // Inherited via iAweByteStream
       virtual int Read(void * _buffer, long _size) override;
       virtual int Write(void * _buffer, long _size) override;
       virtual int Seek(long _offset, eStreamOrigin _origin) override;
       virtual bool Eof() override;
       virtual long Length() override;
       virtual long Position() override;
       virtual void * GetBuffer() override;
	   K_IMPL_REFCOUNTED;
protected:
       long m_Size;
       BYTE* m_pBuffer;
       long m_Pos;
};

