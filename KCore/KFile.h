#pragma once
#include "iKStream.h"
#include "KLibraryDef.h"

class KFile : public iKFile
{
public:
	KFile(const char* pPath);
	virtual ~KFile();

	// Inherited via iAweByteStream
	virtual int Read(void * _buffer, long _size) override;
	virtual int Write(void * _buffer, long _size) override;
	virtual int Seek(long _offset, eStreamOrigin _origin) override;
	virtual bool Eof() override;
	virtual long Length() override;
	virtual long Position() override;
	virtual void * GetBuffer() override;

	K_IMPL_REFCOUNTED;

private:
	HANDLE m_hFile;
	std::string m_Name;
	LONG m_iPosition;

	// Inherited via iAweFile
	virtual const char * GetPath() override;
	virtual bool Open(eFileAccessMode _accessMode) override;
	virtual void Close() override;
	virtual void Flush() override;
	virtual void * GetHandle() override;
};

