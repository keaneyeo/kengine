#pragma once
#include "iKRawMesh.h"

class KRawMesh : public iKRawMesh
{
public:
	KRawMesh(UINT nbvertices, UINT nbindices);
	virtual ~KRawMesh();

	// Inherited via iAweRawMesh
	virtual void SetDataIndices(iKByteStream * stm, bool cached) override;
	virtual void SetDataPosition(iKByteStream * stm, bool cached) override;
	virtual void SetDataUV0(iKByteStream * stm, bool cached) override;
	virtual void SetDataNormals(iKByteStream * stm, bool cached) override;
	virtual void SetDataBinormalTangent(iKByteStream * bnstm, iKByteStream * tstm, bool cached)override;

	INT GetMeshLayout();
	UINT GetNbVertices();
	UINT GetNbIndices();
	UINT GetStrideSize();

	void GetVertices(iKByteStream * buffer);
	void GetIndices(iKByteStream * buffer);

	void SetName(const char * name);
	const char * GetName();

	K_IMPL_REFCOUNTED;

protected:
	iKByteStream * m_pDataIndices = NULL;
	long	m_nStreamIndices = 0;

	iKByteStream*		m_pDataPosition = NULL;
	long	m_nStreamPosition = 0;

	iKByteStream*		m_pDataUV0 = NULL;
	long	m_nStreamUV0 = 0;

	iKByteStream*		m_pDataNormals = NULL;
	long	m_nStreamNormals = 0;

	iKByteStream*		m_pDataBinormals = NULL;
	long	m_nStreamBinormals = 0;

	iKByteStream*		m_pDataTangents = NULL;
	long	m_nStreamTangents = 0;

	UINT	m_nNbVertices;
	UINT	m_nNbIndices;

	std::string _pName;
};
