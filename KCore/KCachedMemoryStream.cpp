#include"stdafx.h"
#include "KCachedMemoryStream.h"

KCachedMemoryStream::KCachedMemoryStream(long size)
{
	START_REF();
	m_Size = size;
	m_pBuffer = new BYTE[m_Size];
	m_Pos = 0;
}
KCachedMemoryStream::KCachedMemoryStream(iKByteStream * file)
{
	START_REF();
	m_Size = file->Length() - file->Position();
	m_pBuffer = new BYTE[m_Size];
	m_Pos = 0;
	K_CopyStream(file, this, m_Size);
	m_Pos = 0;
}
KCachedMemoryStream::~KCachedMemoryStream()
{
	SAFE_DELETE_ARRAY(m_pBuffer);
}
int KCachedMemoryStream::Read(void * _buffer, long _size)
{
	memcpy(_buffer, &m_pBuffer[m_Pos], _size);
	m_Pos += _size;
	return _size;
}
int KCachedMemoryStream::Write(void * _buffer, long _size)
{
	memcpy(&m_pBuffer[m_Pos], _buffer, _size);
	m_Pos += _size;
	return _size;
}
int KCachedMemoryStream::Seek(long _offset, eStreamOrigin _origin)
{
	switch (_origin)
	{
	case foStart:
		if (_offset > m_Size || _offset < 0) return -1;
		m_Pos = _offset;
		break;
	case foCurrent:
		if (m_Pos + _offset > m_Size || m_Pos + _offset < 0) return -1;
		m_Pos += _offset;
		break;
	case foEnd:
		if (m_Size + _offset > m_Size || m_Size + _offset < 0) return -1;
		m_Pos = m_Size + _offset;
		break;
	default: return -1;
	}
	return 0;
}
bool KCachedMemoryStream::Eof()
{
	return m_Pos >= m_Size ? TRUE : FALSE;
}
long KCachedMemoryStream::Length()
{
	return m_Size;
}
long KCachedMemoryStream::Position()
{
	return m_Pos;
}
void * KCachedMemoryStream::GetBuffer()
{
	return m_pBuffer;
}
extern "C"
{
	K_API long K_CopyStream(iKByteStream* srcStream, iKByteStream* outStream, long size)
	{
		const long chunksize = 1024;
		BYTE temp[chunksize];
		long read = 0;
		long total = 0;
		long readgrain = (chunksize > size) ? size : chunksize;
		while ((read = srcStream->Read(temp, readgrain)) > 0)
		{
			outStream->Write(temp, read);
			total += read;
			size -= read;
			readgrain = (chunksize > size) ? size : chunksize;
			if (srcStream->Eof())
				break;
		}
		return total;
	}
	K_API iKByteStream* K_CacheStream(iKByteStream* srcStream, long size = -1)
	{
		iKByteStream* stm = NULL;
		if (size > 0)
		{
			stm = new KCachedMemoryStream(size);
			K_CopyStream(srcStream, stm, size);
			stm->Seek(0, eStreamOrigin::foStart);
		}
		else
		{
			stm = new KCachedMemoryStream(srcStream);
		}
		return stm;
	}
	K_API iKByteStream* K_CacheBuffer(void* srcStream, long size)
	{
		iKByteStream* stm = new KCachedMemoryStream(size);
		stm->Write(srcStream, size);
		stm->Seek(0, eStreamOrigin::foStart);
		return stm;
	}
	K_API iKByteStream* K_CreateCached(long size)
	{
		return new KCachedMemoryStream(size);
	}
}
