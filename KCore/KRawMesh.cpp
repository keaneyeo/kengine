#include "stdafx.h"
#include "KRawMesh.h"
#include "iKRenderer.h"
#include "xmath\XMath.h"
using namespace XMath;

KRawMesh::KRawMesh(UINT nbvertices, UINT nbindices)
{
	START_REF();
	m_nNbVertices = nbvertices;
	m_nNbIndices = nbindices;
}


KRawMesh::~KRawMesh()
{
	K_SAFERELEASE(m_pDataPosition);
	K_SAFERELEASE(m_pDataUV0);
}

void KRawMesh::SetDataPosition(iKByteStream * stm, bool cached)
{
	if (cached)
	{
		m_pDataPosition = K_CacheStream(stm, m_nNbVertices * sizeof(XMFLOAT3));
	}
	else
	{
		m_nStreamPosition = stm->Position();
		m_pDataPosition = stm;
		stm->AddRef();
	}
}

void KRawMesh::SetDataUV0(iKByteStream * stm, bool cached)
{
	if (cached)
	{
		m_pDataUV0 = K_CacheStream(stm, m_nNbVertices * sizeof(XMFLOAT2));
	}
	else
	{
		m_nStreamUV0 = stm->Position();
		m_pDataUV0 = stm;
		stm->AddRef();
	}
}

void KRawMesh::SetDataNormals(iKByteStream * stm, bool cached)
{
	if (cached)
	{
		m_pDataNormals = K_CacheStream(stm, m_nNbVertices * sizeof(XMFLOAT3));
	}
	else
	{
		m_nStreamNormals = stm->Position();
		m_pDataNormals = stm;
		stm->AddRef();
	}
}

INT KRawMesh::GetMeshLayout()
{
	INT layout = eKLayoutFlag::Empty;
	if (m_pDataPosition != NULL)
		layout |= eKLayoutFlag::Position;
	if (m_pDataUV0 != NULL)
		layout |= eKLayoutFlag::UV0;
	if (m_pDataNormals != NULL)
	{
		layout |= eKLayoutFlag::Normal;
		if (m_pDataBinormals != NULL)
			layout |= eKLayoutFlag::TangentBinormal;
	}
	return layout;
}

UINT KRawMesh::GetNbVertices()
{
	return m_nNbVertices;
}

UINT KRawMesh::GetNbIndices()
{
	return m_nNbIndices;
}

UINT KRawMesh::GetStrideSize()
{
	UINT size = 0;
	if (m_pDataPosition != NULL)
		size += sizeof(XMFLOAT3);
	if (m_pDataUV0 != NULL)
		size += sizeof(XMFLOAT2);
	if (m_pDataNormals != NULL)
	{
		size += sizeof(XMFLOAT3);
		if (m_pDataBinormals != NULL)
			size += sizeof(XMFLOAT3) * 2;// and tangent
	}
	return size;
}

void KRawMesh::SetDataIndices(iKByteStream * stm, bool cached)
{
	if (cached)
	{
		m_pDataIndices = K_CacheStream(stm, m_nNbIndices * sizeof(UINT));
	}
	else
	{
		m_nStreamIndices = stm->Position();
		m_pDataIndices = stm;
		stm->AddRef();
	}
}

void KRawMesh::GetVertices(iKByteStream * buffer)
{
	long locpos = m_nStreamPosition;
	long locuv = m_nStreamUV0;
	long locnormal = m_nStreamNormals;
	long locbinormal = m_nStreamBinormals;
	long loctangent = m_nStreamTangents;
	for (UINT i = 0; i < m_nNbVertices; i++)
	{
		if (m_pDataPosition != NULL)
		{
			m_pDataPosition->Seek(locpos, eStreamOrigin::foStart);
			XMFLOAT3 p;
			m_pDataPosition->Read(&p, sizeof(XMFLOAT3));
			buffer->Write(&p, sizeof(XMFLOAT3));
			locpos += sizeof(XMFLOAT3);
		}
		if (m_pDataUV0 != NULL)
		{
			m_pDataUV0->Seek(locuv, eStreamOrigin::foStart);
			XMFLOAT2 p;
			m_pDataUV0->Read(&p, sizeof(XMFLOAT2));
			buffer->Write(&p, sizeof(XMFLOAT2));
			locuv += sizeof(XMFLOAT2);
		}
		if (m_pDataNormals != NULL)
		{
			m_pDataNormals->Seek(locnormal, eStreamOrigin::foStart);
			XMFLOAT3 p;
			m_pDataNormals->Read(&p, sizeof(XMFLOAT3));
			buffer->Write(&p, sizeof(XMFLOAT3));
			locnormal += sizeof(XMFLOAT3);
		}
		if (m_pDataBinormals != NULL && m_pDataNormals != NULL)
		{
			m_pDataBinormals->Seek(locbinormal, eStreamOrigin::foStart);
			XMFLOAT3 p;
			m_pDataBinormals->Read(&p, sizeof(XMFLOAT3));
			buffer->Write(&p, sizeof(XMFLOAT3));
			locbinormal += sizeof(XMFLOAT3);

			m_pDataTangents->Seek(loctangent, eStreamOrigin::foStart);
			m_pDataTangents->Read(&p, sizeof(XMFLOAT3));
			buffer->Write(&p, sizeof(XMFLOAT3));
			loctangent += sizeof(XMFLOAT3);
		}
	}
}

void KRawMesh::GetIndices(iKByteStream * buffer)
{
	m_pDataIndices->Seek(m_nStreamIndices, eStreamOrigin::foStart);
	K_CopyStream(m_pDataIndices, buffer, sizeof(UINT) * m_nNbIndices);
}

void KRawMesh::SetName(const char * name)
{
	_pName = name;
}

const char * KRawMesh::GetName()
{
	return _pName.c_str();
}

void KRawMesh::SetDataBinormalTangent(iKByteStream * bnstm, iKByteStream * tstm, bool cached)
{
	if (cached)
	{
		m_pDataBinormals = K_CacheStream(bnstm, m_nNbVertices * sizeof(XMFLOAT3));
		m_pDataTangents = K_CacheStream(tstm, m_nNbVertices * sizeof(XMFLOAT3));
	}
	else
	{
		m_nStreamBinormals = bnstm->Position();
		m_pDataBinormals = bnstm;
		bnstm->AddRef();

		m_nStreamTangents = tstm->Position();
		m_pDataTangents = tstm;
		tstm->AddRef();
	}
}

iKRawMesh* CreateRawMesh(UINT nbvertices, UINT nbindices)
{
	return new KRawMesh(nbvertices, nbindices);
}
