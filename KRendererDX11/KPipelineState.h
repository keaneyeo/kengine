#pragma once
#include "iKPipelineState.h"

///Class to hold pipline state Dx11
class KPipelineState : public iKPipelineState
{
public:
	KPipelineState();
	~KPipelineState();
	///Constant buffer
	ID3D11Buffer* _pConstantBuffer;
	///Used to set  the world space.
	virtual void SetWorld(const XMMATRIX & matrix) override;
	///Used to set the projection space.
	virtual void SetProjection(const XMMATRIX & matrix) override;
	///Used to set the view space. pos is the view pos, at is the view height, up is the direction.
	virtual void SetView(XMVECTOR pos, XMVECTOR at, XMVECTOR up) override;
	///Used to update the constant buffer
	virtual void Draw() override;
	///Used for rotating mesh. t is deltaTime.
	virtual void SetWorldRotation(const float& t) override;
	///Initialize Constant Buffer
	bool InitConstantBuffer();
	///World space
	XMMATRIX _world;
	///Projection space
	XMMATRIX _projection;
	///View space
	XMMATRIX _view;
};

///Struct for constant buffer
struct MyConstantBuffer
{
	XMMATRIX World;
	XMMATRIX View;
	XMMATRIX Projection;
};
