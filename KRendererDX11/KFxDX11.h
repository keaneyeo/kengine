#pragma once
#include "iKFx.h"

///Class to hold shader Dx11
class KFxDX11 : public iKFx
{
public:
	KFxDX11();
	virtual ~KFxDX11();
	///Display the shader
	virtual void Draw() override;
	///Initialize the shader. shaderPath is the filepath of the shader.
	virtual bool Init(const wchar_t* shaderPath) override;
	///Used to release the vertex and pixel buffers.
	virtual void Clear() override;
	///Blob
	ID3DBlob *VS, *PS, *error;
	ID3D11InputLayout* _pInputLayout = NULL;
	ID3D11VertexShader* _pVShader;
	ID3D11PixelShader* _pPShader;
};

