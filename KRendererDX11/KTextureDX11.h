#pragma once
#include "iKTexture.h"

///Class to hold Texture with Dx11
class KTextureDX11 : public iKTexture
{
public:
	KTextureDX11();
	virtual ~KTextureDX11();
	///To display the textures 
	virtual void Draw() override;
	///To initialize the texture. textureName is the file path, type is the type of texture.
	virtual bool Init(const wchar_t* textureName, TextureType type) override;
	///Used to release the texture
	virtual void Clear() override;
	ID3D11ShaderResourceView* _texture;
	///Type of the texture (diffuse, normal or specular)
	TextureType _type;
};

