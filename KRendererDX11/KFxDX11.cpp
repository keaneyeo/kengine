#include "stdafx.h"
#include "KFxDX11.h"
#include "KRendererDX11.h"

KFxDX11::KFxDX11()
{
}


KFxDX11::~KFxDX11()
{
}

void KFxDX11::Draw()
{
	ID3D11DeviceContext* deviceContext = KRendererDX11::_instance->_pDeviceContext;
	deviceContext->IASetInputLayout(_pInputLayout);
	deviceContext->VSSetShader(_pVShader, 0, 0);
	deviceContext->PSSetShader(_pPShader, 0, 0);
}

bool KFxDX11::Init(const wchar_t * shaderPath)
{
	HRESULT hr = D3DCompileFromFile(shaderPath, 0, 0, "VShader", "vs_4_0",
		0, 0, &VS, &error);
	if (FAILED(hr))
		return false;

	hr = D3DCompileFromFile(shaderPath, 0, 0, "PShader", "ps_4_0",
		0, 0, &PS, &error);
	if (FAILED(hr))
		return false;

	ID3D11Device* device = KRendererDX11::_instance->_pD3DDevice;
	hr = device->CreateVertexShader(VS->GetBufferPointer(), VS->GetBufferSize(),
		NULL, &_pVShader);

	if (FAILED(hr)) return false;

	hr = device->CreatePixelShader(PS->GetBufferPointer(), PS->GetBufferSize(),
		NULL, &_pPShader);

	if (FAILED(hr)) return false;


	//TODO input layout
	D3D11_INPUT_ELEMENT_DESC ied[]
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA },
		/*{"COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT,
		D3D11_INPUT_PER_VERTEX_DATA},*/
	{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT,
	D3D11_INPUT_PER_VERTEX_DATA },
	{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT,
	D3D11_INPUT_PER_VERTEX_DATA },
	{ "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT,
	D3D11_INPUT_PER_VERTEX_DATA },
	{ "BINORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT,
	D3D11_INPUT_PER_VERTEX_DATA },
	};

	hr = device->CreateInputLayout(ied, 5, VS->GetBufferPointer(), VS->GetBufferSize(),
		&_pInputLayout);

	VS->Release();
	PS->Release();
	if (FAILED(hr)) return false;

	return true;
}

void KFxDX11::Clear()
{
	K_SAFERELEASE(_pPShader);
	K_SAFERELEASE(_pVShader);
}

iKFx* CreateShader(const wchar_t* shaderPath)
{
	KFxDX11* shader = new KFxDX11();
	K_ASSERT(shader->Init(shaderPath));
	return shader;
}