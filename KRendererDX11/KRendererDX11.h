#pragma once
#include "iKRenderer.h"
#include <d3d11.h>

///Class to hold the renderer Dx11
class KRendererDX11 : public iKRenderer
{
public:
	KRendererDX11();
	virtual ~KRendererDX11();
	// Inherited via iKRenderer
	///Initialize function. Windows params to determine size of window.
	virtual bool Initialize(const WindowParams &params) override;
	///Initialize Sampler
	virtual bool InitSampler() override;
	///Initialize RasterState
	virtual bool InitRasterState(const WindowParams &params) override;
	///Set the sampler state
	virtual void SetSampler() override;
	///Set the depth view 
	virtual void SetDepth() override;
	///Set topology
	virtual void SetTopology() override;
	///Used to clear render target view and clear depth stencil view
	virtual void Clear() override;
	///Used to present 
	virtual void Present() override;
	///Instance of renderer
	static KRendererDX11* _instance;
	ID3D11Device * _pD3DDevice = NULL;
	ID3D11RenderTargetView* _pMainRTV = NULL;
	IDXGISwapChain* _pSwapChain = NULL;
	ID3D11DeviceContext* _pDeviceContext = NULL;
	UINT _SDKVersion = D3D11_SDK_VERSION;
	ID3D11SamplerState* _pSamplerState = NULL;
	ID3D11DepthStencilView* _pDepthStencilView = NULL;
};