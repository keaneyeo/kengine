#pragma once
#include "iKMesh.h"

///Used to hold Mesh Dx11
class KMeshDX11 : public iKMesh
{
public:
	KMeshDX11();
	~KMeshDX11();
	///Initialize mesh with a raw mesh
	bool Init(iKRawMesh* params);
	///Used to draw the mesh
	virtual void Draw() override;
	// Inherited via iKMesh
	///Vertex buffer
	ID3D11Buffer * VB;
	///Index buffer
	ID3D11Buffer * IB;
	///Number of vertices
	unsigned int NBVertices;
	///Number of indices
	unsigned int NBIndices;
	///Stride size
	unsigned int StrideSize;
};



