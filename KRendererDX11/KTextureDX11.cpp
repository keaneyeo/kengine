#include "stdafx.h"
#include "KTextureDX11.h"
#include "DDSTextureLoader.h"
#include "KRendererDX11.h"

KTextureDX11::KTextureDX11()
{
}


KTextureDX11::~KTextureDX11()
{
}

void KTextureDX11::Draw()
{
	ID3D11DeviceContext* dc = KRendererDX11::_instance->_pDeviceContext;
	dc->PSSetShaderResources((UINT)_type, 1, &_texture);
}

bool KTextureDX11::Init(const wchar_t * textureName, TextureType type)
{
	ID3D11Device* device = KRendererDX11::_instance->_pD3DDevice;
	HRESULT hr = DirectX::CreateDDSTextureFromFile(device, textureName, NULL, &_texture);
	if (FAILED(hr))
		return false;
	_type = type;
	return true;
}

void KTextureDX11::Clear()
{
	K_SAFERELEASE(_texture);
}

iKTexture* CreateTexture(const wchar_t* textureName, TextureType type) 
{
	KTextureDX11* texture = new KTextureDX11();
	K_ASSERT(texture->Init(textureName, type));
	return texture;
}
