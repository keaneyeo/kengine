#include "stdafx.h"
#include "KPipelineState.h"
#include "KRendererDX11.h"

KPipelineState::KPipelineState()
{
}


KPipelineState::~KPipelineState()
{
}

void KPipelineState::SetWorld(const XMMATRIX & matrix)
{
	_world = matrix;
}

void KPipelineState::SetProjection(const XMMATRIX & matrix)
{
	_projection = matrix;
}

void KPipelineState::SetView(XMVECTOR pos, XMVECTOR at, XMVECTOR up)
{
	_view = XMMatrixLookAtLH(pos, at, up);
}

void KPipelineState::Draw()
{
	ID3D11DeviceContext* deviceContext = KRendererDX11::_instance->_pDeviceContext;
	MyConstantBuffer cb;
	cb.World = XMMatrixTranspose(_world);
	cb.View = XMMatrixTranspose(_view);
	cb.Projection = XMMatrixTranspose(_projection);
	deviceContext->UpdateSubresource(_pConstantBuffer, NULL, NULL, &cb, NULL, NULL);
	deviceContext->VSSetConstantBuffers(0, 1, &_pConstantBuffer);
}

void KPipelineState::SetWorldRotation(const float & t)
{
	_world = XMMatrixRotationY(t);
	XMMATRIX tempcorrect = XMMatrixRotationX(-90 * XM_PI / 180.0f);
	_world = tempcorrect * _world;
}

bool KPipelineState::InitConstantBuffer()
{
	D3D11_BUFFER_DESC bd;
	memset(&bd, 0, sizeof(D3D11_BUFFER_DESC));

	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(MyConstantBuffer);
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = 0;

	ID3D11Device* device = KRendererDX11::_instance->_pD3DDevice;
	HRESULT hr = device->CreateBuffer(&bd, NULL, &_pConstantBuffer);
	if (FAILED(hr)) return false;

	return true;
}

iKPipelineState* CreatePipelineState(XMVECTOR pos, XMVECTOR at, XMVECTOR up)
{
	KPipelineState* pipelinestate = new KPipelineState();
	K_ASSERT(pipelinestate->InitConstantBuffer());
	XMMATRIX matrix = XMMatrixIdentity();
	pipelinestate->SetWorld(matrix);
	pipelinestate->SetProjection(XMMatrixPerspectiveFovLH(XM_PIDIV2, 640.0f / 480.0f, 0.01f, 500.0f));
	pipelinestate->SetView(pos, at, up);
	return pipelinestate;
}