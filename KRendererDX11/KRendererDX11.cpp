// KRendererDX11.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "KRendererDX11.h"

KRendererDX11* KRendererDX11::_instance = NULL;

KRendererDX11::KRendererDX11()
{
	K_ASSERT(_instance == NULL);
	_instance = this;
}

KRendererDX11::~KRendererDX11()
{
	K_SAFERELEASE(_pMainRTV);
	K_SAFERELEASE(_pDeviceContext);
	K_SAFERELEASE(_pSwapChain);
	K_SAFERELEASE(_pD3DDevice);
	_instance = NULL;
}

bool KRendererDX11::Initialize(const WindowParams &params)
{

	unsigned int createdeviceflag = 0;
#ifdef _DEBUG
	createdeviceflag |= D3D11_CREATE_DEVICE_DEBUG;
#endif
	DXGI_SWAP_CHAIN_DESC swapdesc;
	memset(&swapdesc, 0, sizeof(DXGI_SWAP_CHAIN_DESC));

	swapdesc.OutputWindow = (HWND)params.Handle;
	swapdesc.Windowed = TRUE;
	swapdesc.BufferCount = 1;
	swapdesc.BufferDesc.Width = params.Width;
	swapdesc.BufferDesc.Height = params.Height;
	swapdesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	swapdesc.BufferDesc.RefreshRate.Numerator = 60;
	swapdesc.BufferDesc.RefreshRate.Denominator = 1;
	swapdesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapdesc.SampleDesc.Count = 1;
	swapdesc.SampleDesc.Quality = 0;
	swapdesc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
	D3D_FEATURE_LEVEL featurelevel = D3D_FEATURE_LEVEL_11_0;
	D3D_FEATURE_LEVEL featurelevel2 = featurelevel;
	HRESULT hr = D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, createdeviceflag,
		&featurelevel, 1, _SDKVersion, &swapdesc, &_pSwapChain, &_pD3DDevice, &featurelevel2, &_pDeviceContext);

	if (FAILED(hr))
		return false;

	K_DEBUGLOG(L"D3D Device create.\n");
	ID3D11Texture2D* pTexture;

	hr = _pSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pTexture);

	if (FAILED(hr))
		return false;

	K_DEBUGLOG(L"Main buffer to texture fetch successful.\n");

	hr = _pD3DDevice->CreateRenderTargetView(pTexture, NULL, &_pMainRTV);

	if (FAILED(hr))
		return false;

	K_DEBUGLOG(L"Main render target created.\n");

	D3D11_VIEWPORT viewport;
	memset(&viewport, 0, sizeof(D3D11_VIEWPORT));

	viewport.TopLeftX = viewport.TopLeftY = 0;
	viewport.Width = (FLOAT)params.Width;
	viewport.Height = (FLOAT)params.Height;
	viewport.MinDepth = 0;
	viewport.MaxDepth = 1;

	_pDeviceContext->RSSetViewports(1, &viewport);

	K_DEBUGLOG(L"Viewport set.\n");
	return true;
}

bool KRendererDX11::InitSampler()
{
	D3D11_SAMPLER_DESC sd;
	memset(&sd, 0, sizeof(D3D11_SAMPLER_DESC));

	sd.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	sd.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	sd.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	sd.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	sd.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	sd.MinLOD = 0;
	sd.MaxLOD = D3D11_FLOAT32_MAX;

	HRESULT hr = _pD3DDevice->CreateSamplerState(&sd, &_pSamplerState);
	if (FAILED(hr)) return false;


	return true;
}

void KRendererDX11::SetSampler() 
{
	_pDeviceContext->PSSetSamplers(0, 1, &_pSamplerState);
}

void KRendererDX11::SetDepth()
{

}

void KRendererDX11::SetTopology()
{
	_pDeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}

bool KRendererDX11::InitRasterState(const WindowParams &params)
{
	HRESULT hr;

	ID3D11Texture2D* depthstencil;
	D3D11_TEXTURE2D_DESC descDepth;
	ZeroMemory(&descDepth, sizeof(descDepth));
	descDepth.Width = params.Width;
	descDepth.Height = params.Height;
	descDepth.MipLevels = 1;
	descDepth.ArraySize = 1;
	descDepth.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	descDepth.SampleDesc.Count = 1;
	descDepth.SampleDesc.Quality = 0;
	descDepth.Usage = D3D11_USAGE_DEFAULT;
	descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	descDepth.CPUAccessFlags = 0;
	descDepth.MiscFlags = 0;
	hr = _pD3DDevice->CreateTexture2D(&descDepth, NULL, &depthstencil);

	if (FAILED(hr)) return false;

	D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
	ZeroMemory(&descDSV, sizeof(descDSV));
	descDSV.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	descDSV.Texture2D.MipSlice = 0;
	hr = _pD3DDevice->CreateDepthStencilView(depthstencil, &descDSV, &_pDepthStencilView);
	depthstencil->Release();

	if (FAILED(hr)) return false;
	return true;
}

void KRendererDX11::Clear()
{
	float clearcolor[4] = { 0.15f,0.15f,0.15f,1 };//0-1 per channnel background renderer colour
	_pDeviceContext->OMSetRenderTargets(1, &_pMainRTV, _pDepthStencilView);
	_pDeviceContext->ClearRenderTargetView(_pMainRTV, clearcolor);
	_pDeviceContext->ClearDepthStencilView(_pDepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);
}

void KRendererDX11::Present()
{
	_pSwapChain->Present(0, 0);
}

iKRenderer* CreateRenderer(const WindowParams& params)
{
	KRendererDX11* renderer = new KRendererDX11();
	K_ASSERT(renderer->Initialize(params));
	K_ASSERT(renderer->InitSampler());
	K_ASSERT(renderer->InitRasterState(params));
	return renderer;
}