#include "stdafx.h"
#include "KMeshDX11.h"
#include "KImporterStuff.h"
#include <d3d11.h>
#include "KRendererDX11.h"

KMeshDX11::KMeshDX11()
{
}


KMeshDX11::~KMeshDX11()
{
}

bool KMeshDX11::Init(iKRawMesh * params)
{
	ID3D11Device* device = KRendererDX11::_instance->_pD3DDevice;
	ID3D11DeviceContext* deviceContext = KRendererDX11::_instance->_pDeviceContext;
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));

	bd.Usage = D3D11_USAGE_DYNAMIC;
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	
	iKRawMesh* mesh = params;
	StrideSize = mesh->GetStrideSize();
	NBVertices = mesh->GetNbVertices();
	NBIndices = mesh->GetNbIndices();
	unsigned int vsize = mesh->GetStrideSize() * NBVertices;
	unsigned int isize = sizeof(unsigned int) * NBIndices;

	iKByteStream* vstm = K_CreateCached(vsize);
	mesh->GetVertices(vstm);

	iKByteStream* istm = K_CreateCached(isize);
	mesh->GetIndices(istm);

	bd.ByteWidth = vsize;
	HRESULT hr = device->CreateBuffer(&bd, NULL, &VB);

	K_ASSERT(SUCCEEDED(hr));

	bd.ByteWidth = isize;
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	hr = device->CreateBuffer(&bd, NULL, &IB);

	K_ASSERT(SUCCEEDED(hr));

	D3D11_MAPPED_SUBRESOURCE ms;
	hr = deviceContext->Map(VB, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &ms);
	memcpy(ms.pData, vstm->GetBuffer(), vstm->Length());
	deviceContext->Unmap(VB, NULL);

	K_ASSERT(SUCCEEDED(hr));

	hr = deviceContext->Map(IB, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &ms);
	memcpy(ms.pData, istm->GetBuffer(), istm->Length());
	deviceContext->Unmap(IB, NULL);

	K_ASSERT(SUCCEEDED(hr));
	//meshes.push_back(md);
	K_SAFERELEASE(vstm);
	K_SAFERELEASE(istm);
	K_SAFERELEASE(mesh);
	return true;
}

///
void KMeshDX11::Draw()
{
	ID3D11DeviceContext* deviceContext = KRendererDX11::_instance->_pDeviceContext;
	deviceContext->IASetIndexBuffer(IB, DXGI_FORMAT_R32_UINT, 0);
	unsigned int offset = 0;
	deviceContext->IASetVertexBuffers(0, 1, &VB, &StrideSize, &offset);
	deviceContext->DrawIndexed(NBIndices, 0, 0);
}

iKMesh* CreateMesh(iKRawMesh* params) 
{
	KMeshDX11* newMesh = new KMeshDX11();
	K_ASSERT(newMesh->Init(params));
	return newMesh;
}
