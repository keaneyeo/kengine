#pragma once

class iKGame
{
public:
	iKGame() {};
	virtual ~iKGame() {};

	virtual bool Load() = 0;
	virtual void Start() = 0;
	virtual void Update() = 0;
	virtual void Stop() = 0;
	virtual void Unload() = 0;
};