#pragma once
#include "iKGame.h"

///Game class
class Game01 : public iKGame
{
public:
	Game01();
	virtual ~Game01();

	// Inherited via iKGame
	///Load all renderer/textures/shader/meshes/pipeline for the game
	virtual bool Load() override;
	///Start the game loop
	virtual void Start() override;
	///Game loop, draw functions should go here
	virtual void Update() override;
	///When the game is stopped
	virtual void Stop() override;
	///Unload all resources
	virtual void Unload() override;
	///Function to calculate DeltaTime
	bool Tick();
	///If game is running, true, else false.
	bool Running = false;
	///DeltaTime
	float t;
};

