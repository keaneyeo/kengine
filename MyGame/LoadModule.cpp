#include "stdafx.h"
#include "LoadModule.h"

CREATERENDERER* CREATERENDERERFUNC = NULL;
CREATEMESH* CREATEMESHFUNC = NULL;
CREATETEXTURE* CREATETEXTUREFUNC = NULL;
CREATESHADER* CREATESHADERFUNC = NULL;
CREATEPIPELINESTATE* CREATEPIPELINESTATEFUNC = NULL;
HMODULE lib = NULL;

iKRenderer * K_CreateRenderer(const WindowParams & params)
{
	if (lib == NULL)
	{
		lib = LoadLibrary(L"KRendererDX11");
	}

	if (CREATERENDERERFUNC == NULL)
	{
		//getting the function pointer from DLL
		CREATERENDERERFUNC = (CREATERENDERER*)GetProcAddress(lib, "CreateRenderer");
	}
	return CREATERENDERERFUNC(params);
}

iKMesh * K_CreateMesh(iKRawMesh * param)
{
	if (lib == NULL)
	{
		lib = LoadLibrary(L"KRendererDX11");
	}

	if (CREATEMESHFUNC == NULL)
	{
		//getting the function pointer from DLL
		CREATEMESHFUNC = (CREATEMESH*)GetProcAddress(lib, "CreateMesh");
	}
	return CREATEMESHFUNC(param);
}

iKTexture * K_CreateTexture(const wchar_t * textureName, TextureType type)
{
	if (lib == NULL)
	{
		lib = LoadLibrary(L"KRendererDX11");
	}

	if (CREATETEXTUREFUNC == NULL)
	{
		//getting the function pointer from DLL
		CREATETEXTUREFUNC = (CREATETEXTURE*)GetProcAddress(lib, "CreateTexture");
	}
	return CREATETEXTUREFUNC(textureName, type);
}

iKFx * K_CreateShader(const wchar_t * shaderPath)
{
	if (lib == NULL)
	{
		lib = LoadLibrary(L"KRendererDX11");
	}

	if (CREATESHADERFUNC == NULL)
	{
		//getting the function pointer from DLL
		CREATESHADERFUNC = (CREATESHADER*)GetProcAddress(lib, "CreateShader");
	}
	return CREATESHADERFUNC(shaderPath);
}

iKPipelineState * K_CreatePipelineState(XMVECTOR pos, XMVECTOR at, XMVECTOR up)
{
	if (lib == NULL)
	{
		lib = LoadLibrary(L"KRendererDX11");
	}

	if (CREATEPIPELINESTATEFUNC == NULL)
	{
		//getting the function pointer from DLL
		CREATEPIPELINESTATEFUNC = (CREATEPIPELINESTATE*)GetProcAddress(lib, "CreatePipelineState");
	}
	return CREATEPIPELINESTATEFUNC(pos, at, up);
}
