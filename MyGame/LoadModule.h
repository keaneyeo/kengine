#pragma once
#include "iKRenderer.h"
#include "iKMesh.h"
#include "iKTexture.h"
#include "iKFx.h"
#include "iKPipelineState.h"

///Load and create renderer
iKRenderer* K_CreateRenderer(const WindowParams& params);
///Load and create mesh
iKMesh* K_CreateMesh(iKRawMesh* param);
///Load and create texture
iKTexture* K_CreateTexture(const wchar_t* textureName, TextureType type);
///Load and create shaderpath
iKFx* K_CreateShader(const wchar_t* shaderPath);
///Load and create pipeline
iKPipelineState* K_CreatePipelineState(XMVECTOR pos, XMVECTOR at, XMVECTOR up);

