#include "stdafx.h"
#include "Game01.h"
#include "iKRenderer.h"
#include "LoadModule.h"
#include "SDL.h"
#include "SDL_syswm.h"
#include "KImporterStuff.h"

#include <vector>

std::vector<iKMesh*> meshes;
XMVECTOR Eye = { 0,100,-150,0 };
XMVECTOR At = { 0,70,0,0 };
XMVECTOR Up = { 0,1,0,0 };

SDL_Event event;

iKRenderer* renderer = NULL;
iKPipelineState* pipeline = NULL;
iKTexture* tDiffuse = NULL;
iKTexture* tNormal = NULL;
iKTexture* tSpecular = NULL;
iKFx* shader = NULL;
SDL_Window* window = NULL;

struct MyVertex
{
public:
	XMFLOAT3 Position;
	XMFLOAT4 Color;
	XMFLOAT2 UV;
};

MyVertex g_MyVerts[]
{
	{ XMFLOAT3(0.5f,0.5f,0.0f), XMFLOAT4(1.0f,0.0f,0.0f,1.0f), XMFLOAT2(0,0) },
	{ XMFLOAT3(0.5f,-0.5f,0.0f), XMFLOAT4(0.0f,1.0f,0.0f,1.0f), XMFLOAT2(1,0) },
	{ XMFLOAT3(-0.5f,-0.5f,0.0f), XMFLOAT4(0.0f,0.0f,1.0f,1.0f), XMFLOAT2(1,1) },
	{ XMFLOAT3(-0.5f,0.5f,0.0f), XMFLOAT4(0.0f,0.0f,1.0f,1.0f), XMFLOAT2(0,1) },
};

Game01::Game01()
{
}


Game01::~Game01()
{
}

bool Game01::Load()
{
	WindowParams p;
	p.Width = 640;
	p.Height = 480;
	if (SDL_Init(SDL_INIT_EVERYTHING) == 0)
	{
		window = SDL_CreateWindow("3D Viewer",
			SDL_WINDOWPOS_CENTERED,
			SDL_WINDOWPOS_CENTERED,
			640, 480, SDL_WINDOW_SHOWN);

		void* windowhandle = NULL;
		SDL_SysWMinfo info;
		SDL_VERSION(&info.version); // initialize info structure with SDL version info
		if (SDL_GetWindowWMInfo(window, &info))
		{
			windowhandle = info.info.win.window;
		}
		p.Handle = windowhandle;
	}
	renderer = K_CreateRenderer(p);
	pipeline = K_CreatePipelineState(Eye, At, Up);

	iKRawMesh* temp[30];
	unsigned int nbmesh = 0;
	ImportMesh("data/Raptor.FBX", temp, nbmesh);
	for (unsigned int i = 0; i < nbmesh; i++)
	{
		iKMesh* mesh = K_CreateMesh(temp[i]);
		meshes.push_back(mesh);
	}
	tDiffuse = K_CreateTexture(L"data/raptor_d.dds", ttDiffuse);
	tNormal = K_CreateTexture(L"data/raptor_nm.dds", ttNormal);
	tSpecular = K_CreateTexture(L"data/raptor_spec.dds", ttSpecular);
	shader = K_CreateShader(L"data/shader.shader");
	return true;
}

void Game01::Start()
{
	Running = true;
}

void Game01::Update()
{	
	pipeline->Draw();
	renderer->Clear();
	shader->Draw();
	tDiffuse->Draw();
	tNormal->Draw();
	tSpecular->Draw();
	renderer->SetSampler();
	pipeline->SetWorldRotation(t);
	unsigned int stride = sizeof(MyVertex);
	unsigned int offset = 0;
	renderer->SetTopology();
	for (size_t i = 0; i < meshes.size(); i++)
	{
		meshes[i]->Draw();
	}
	renderer->Present();
}

void Game01::Stop()
{
	tDiffuse->Clear();
	tNormal->Clear();
	tSpecular->Clear();
	shader->Clear();
	Running = false;
}

void Game01::Unload()
{
	SDL_DestroyWindow(window);
	SDL_Quit();
}

bool Game01::Tick()
{
	t = 0;
	static float ticks_elapsed = 0;
	unsigned int ticks = SDL_GetTicks();
	t = (ticks - ticks_elapsed) / 1000.0f;
	return Running;
}

Game01 _game;

int main(int arg, char* arv[])
{
	_game.Load();
	_game.Start();
	while (_game.Tick())
	{
		while (SDL_PollEvent(&event))
		{
			switch (event.type)
			{
			case SDL_QUIT:
				
				_game.Stop();
				break;
			}
		}
		_game.Update();
	}
	_game.Unload();
	return 0;
}