#pragma once

#ifdef K_API_EXPORTS
#define K_API __declspec(dllexport)
#else
#define K_API __declspec(dllimport)
#endif