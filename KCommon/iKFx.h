#pragma once
#include "KBaseDef.h"
#include "KLibraryDef.h"

///Class for Shaders
class iKFx
{
public:
	///Used to initialize Shader file. shaderPath is the shader file path.
	virtual bool Init(const wchar_t* shaderPath) = 0;
	///Display shader.
	virtual void Draw() = 0;
	///To release pixel and vertex shaders.
	virtual void Clear() = 0;
};

extern "C"
{
	//function pointer type
	typedef iKFx* (CREATESHADER)(const wchar_t* textureName);

	//the signature
	K_API iKFx* CreateShader(const wchar_t* textureName);
}