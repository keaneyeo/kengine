#pragma once
#include "KBaseDef.h"
#include "KLibraryDef.h"

struct WindowParams
{
public:
	unsigned int Width;
	unsigned int Height;
	bool Windowed;
	void* Handle;
};

enum eKLayoutFlag
{
	Empty = 0,
	Position = 1,
	UV0 = 1 << 1,
	Normal = 1 << 2,
	TangentBinormal = 1 << 3,
};
class iKRenderer
{
public:
	virtual ~iKRenderer(){}
	virtual bool Initialize(const WindowParams& params) = 0;
	virtual bool InitSampler() = 0;
	virtual bool InitRasterState(const WindowParams &params) = 0;
	virtual void SetSampler() = 0;
	virtual void SetDepth() = 0;
	virtual void SetTopology() = 0;
	virtual void Clear() = 0;
	virtual void Present() = 0;

};

extern "C"
{
	//function pointer type
	typedef iKRenderer* (CREATERENDERER)(const WindowParams& params);

	//the signature
	K_API iKRenderer* CreateRenderer(const WindowParams& params);
}