#pragma once
#include "KBaseDef.h"
#include "KLibraryDef.h"

enum TextureType
{
	ttDiffuse = 0,
	ttNormal,
	ttSpecular,
};
class iKTexture
{
public:
	virtual ~iKTexture(){}
	virtual void Draw() = 0;
	virtual bool Init(const wchar_t* textureName, TextureType type) = 0;
	virtual void Clear() = 0;
};


extern "C"
{
	//function pointer type
	typedef iKTexture* (CREATETEXTURE)(const wchar_t* textureName, TextureType type);

	//the signature
	K_API iKTexture* CreateTexture(const wchar_t* textureName, TextureType type);
}
