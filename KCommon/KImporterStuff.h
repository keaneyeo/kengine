#pragma once
#include "iKRawMesh.h"

extern "C"
{
	typedef void (IMPORTMESH)(const char* path, iKRawMesh** meshes, unsigned int& nbmeshes);
	K_API void ImportMesh(const char* path, iKRawMesh** meshes, unsigned int& nbmeshes);

}