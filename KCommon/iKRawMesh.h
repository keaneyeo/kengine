#pragma once

#include "iKStream.h"

///Raw mesh class
class iKRawMesh
{
public:
	virtual ~iKRawMesh() {}
	virtual void SetDataIndices(iKByteStream* stm, bool cached) = 0;
	virtual void SetDataPosition(iKByteStream* stm, bool cached) = 0;
	virtual void SetDataUV0(iKByteStream* stm, bool cached) = 0;
	virtual void SetDataNormals(iKByteStream* stm, bool cached) = 0;
	virtual void SetDataBinormalTangent(iKByteStream* bnstm, iKByteStream* tstm, bool cached) = 0;
	virtual void SetName(const char* name) = 0;
	virtual const char* GetName() = 0;

	virtual INT GetMeshLayout() = 0;
	virtual UINT GetNbVertices() = 0;
	virtual UINT GetNbIndices() = 0;
	virtual UINT GetStrideSize() = 0;

	virtual void GetVertices(iKByteStream* buffer) = 0;
	virtual void GetIndices(iKByteStream* buffer) = 0;
	K_ABSTRACT_REFCOUNTED;
};

extern "C"
{
	typedef iKRawMesh* (CREATERAWMESH)(UINT nbvertices, UINT nbindices);
	K_API iKRawMesh* CreateRawMesh(UINT nbvertices, UINT nbindices);
}