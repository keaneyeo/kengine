#pragma once
#include "KBaseDef.h"
#include "KLibraryDef.h"

class iKRawMesh;

///Class to hold mesh
class iKMesh 
{
public:
	virtual ~iKMesh(){}
	///Function to display mesh
	virtual void Draw() = 0;
};

extern "C"
{
	//function pointer type
	typedef iKMesh* (CREATEMESH)(iKRawMesh* params);

	//the signature
	K_API iKMesh* CreateMesh(iKRawMesh* params);
}