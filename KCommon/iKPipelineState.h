#pragma once
#include "KBaseDef.h"
#include "KLibraryDef.h"
#include "xmath\XMath.h"

using namespace XMath;

///Class to hold pipeline state
class iKPipelineState
{
public:
	///Used to set  the world space.
	virtual void SetWorld(const XMMATRIX& matrix) = 0;
	///Used to set the projection space.
	virtual void SetProjection(const XMMATRIX& matrix) = 0;
	///Used to set the view space. pos is the view pos, at is the view height, up is the direction.
	virtual void SetView(XMVECTOR pos, XMVECTOR at, XMVECTOR up) = 0;
	///Used to update the constant buffer
	virtual void Draw() = 0;
	///Used for rotating mesh. t is deltaTime.
	virtual void SetWorldRotation(const float& t) = 0;
};

extern "C"
{
	//function pointer type
	typedef iKPipelineState* (CREATEPIPELINESTATE)(XMVECTOR pos, XMVECTOR at, XMVECTOR up);

	//the signature
	K_API iKPipelineState* CreatePipelineState(XMVECTOR pos, XMVECTOR at, XMVECTOR up);
}