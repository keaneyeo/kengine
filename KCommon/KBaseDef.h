#pragma once

#include <assert.h>

#define K_ASSERT(t) assert(t)

#define K_DEBUGLOG(s) printf("%ls", s);

#define K_SAFERELEASE(p) if(p!=NULL)p->Release(); p = NULL;

#define K_COMMONABSTRACT_DEF \
virtual void AddRef() = 0; \
virtual void Release() = 0;

#define K_ABSTRACT_REFCOUNTED \
	virtual int StartRef() = 0;\
	virtual int AddRef() = 0; \
	virtual int Release() = 0;\
	virtual int GetRefCount() = 0;

#define K_IMPL_REFCOUNTED\
	virtual int StartRef() override { return m_Ref = 1; } \
	virtual int AddRef() override { return ++m_Ref; } \
	virtual int Release() override { if(--m_Ref <= 0) \
										{delete this; return 0;}\
									return m_Ref; } \
	virtual int GetRefCount() override { return m_Ref; } \
	protected: int m_Ref = 0;

#define START_REF() this->StartRef();

#define SAFE_DELETE_ARRAY(x)\
if(x) delete [] x; \
x = NULL;