#pragma once
#include "KLibraryDef.h"
#include "KBaseDef.h"

enum eFileAccessMode
{
	famCreate = 0x01,
	famRead = 0x02,
	famWrite = 0x04,
	famReadWrite = 0x06,
};
enum eStreamOrigin
{
	foStart = 0,
	foCurrent = 1,
	foEnd = 2
};
struct iKByteStream
{
	K_ABSTRACT_REFCOUNTED;

public:
	~iKByteStream() { }
	virtual int Read(void *_buffer, long _size) = 0;
	virtual int Write(void *_buffer, long _size) = 0;
	virtual int Seek(long _offset, eStreamOrigin _origin) = 0;
	virtual bool Eof() = 0;
	virtual long Length() = 0;
	virtual long Position() = 0;
	virtual void* GetBuffer() = 0;
};

struct iKFile : public iKByteStream
{
public:
	virtual ~iKFile() { }
	virtual const char* GetPath() = 0;
	virtual bool Open(eFileAccessMode _accessMode) = 0;
	virtual void Close() = 0;
	virtual void Flush() = 0;
	virtual void* GetHandle() = 0;
protected:
	virtual void* GetBuffer() = 0;
};

extern "C"
{
	typedef iKFile *(LOADFILEFUNC)(const char* _path, eFileAccessMode _accessMode);
	K_API iKFile *K_LoadFile(const char* _path, eFileAccessMode _accessMode);

	typedef long(COPYSTREAM)(iKByteStream* _srcStream, iKByteStream* _outStream, long _size);
	K_API long K_CopyStream(iKByteStream* _srcStream, iKByteStream* _outStream, long _size);

	typedef iKByteStream*(CACHESTREAM)(iKByteStream* _srcStream, long size);
	K_API iKByteStream* K_CacheStream(iKByteStream* _srcStream, long size);

	typedef iKByteStream*(CACHEBUFFER)(void* srcStream, long size);
	K_API iKByteStream* K_CacheBuffer(void* srcStream, long size);

	typedef iKByteStream*(CREATECACHED)(long size);
	K_API iKByteStream* K_CreateCached(long size);
}
