// XGImporter.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"

#include "KImporterStuff.h"
#include "iKRawMesh.h"
#include "assimp\Importer.hpp"
#include "assimp\scene.h"
#include "assimp\postprocess.h"

using namespace XMath;

void ImportMesh(const char* path, iKRawMesh** meshes, unsigned int& nbmeshes)
{
	Assimp::Importer importer;
	const aiScene* scene = importer.ReadFile(path,
		aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		aiProcess_JoinIdenticalVertices |
		aiProcess_SortByPType | aiProcess_FlipUVs);

	if (scene->HasMeshes())
	{
		nbmeshes = scene->mNumMeshes;
		for (UINT i = 0; i<nbmeshes; i++)
		{
			aiMesh* mesh = scene->mMeshes[i];

			UINT nbVertices = mesh->mNumVertices;
			UINT nbFaces = mesh->mNumFaces;
			UINT nbIndices = nbFaces * 3;//assuming each face is a triangle
			iKRawMesh* pMesh = CreateRawMesh(nbVertices, nbIndices);
			pMesh->SetName(mesh->mName.data);
			meshes[i] = pMesh;
			aiVector3D *pSourceVerts = mesh->mVertices;
			aiVector3D *pNormals = mesh->mNormals;
			aiVector3D *pUV = mesh->mTextureCoords[0];
			aiVector3D *pBinormals = mesh->mBitangents;
			aiVector3D *pTangents = mesh->mTangents;
			iKByteStream* stm = K_CreateCached(nbVertices * sizeof(XMFLOAT3));
			iKByteStream* stmColor0 = K_CreateCached(nbVertices * sizeof(XMFLOAT4));
			iKByteStream* stmNormal = K_CreateCached(nbVertices * sizeof(XMFLOAT3));
			iKByteStream* stmUV = K_CreateCached(nbVertices * sizeof(XMFLOAT2));
			iKByteStream* stmBN = K_CreateCached(nbVertices * sizeof(XMFLOAT3));
			iKByteStream* stmT = K_CreateCached(nbVertices * sizeof(XMFLOAT3));
			for (UINT v = 0; v<nbVertices; v++)
			{
				stm->Write(&pSourceVerts[v], sizeof(XMFLOAT3));
				stmUV->Write(&pUV[v], sizeof(XMFLOAT2));

				if (pNormals)
					stmNormal->Write(&pNormals[v], sizeof(XMFLOAT3));

				if (pBinormals && pTangents)
				{
					stmBN->Write(&pBinormals[v], sizeof(XMFLOAT3));
					stmT->Write(&pTangents[v], sizeof(XMFLOAT3));
				}
			}
			stm->Seek(0, eStreamOrigin::foStart);
			stmUV->Seek(0, eStreamOrigin::foStart);
			stmColor0->Seek(0, eStreamOrigin::foStart);
			stmNormal->Seek(0, eStreamOrigin::foStart);
			stmBN->Seek(0, eStreamOrigin::foStart);
			stmT->Seek(0, eStreamOrigin::foStart);

			pMesh->SetDataPosition(stm, true);
			pMesh->SetDataUV0(stmUV, true);
			if (pNormals)
				pMesh->SetDataNormals(stmNormal, true);
			if (pBinormals && pTangents)
				pMesh->SetDataBinormalTangent(stmBN, stmT, true);

			K_SAFERELEASE(stm);
			K_SAFERELEASE(stmUV);
			K_SAFERELEASE(stmColor0);
			K_SAFERELEASE(stmNormal);
			K_SAFERELEASE(stmBN);
			K_SAFERELEASE(stmT);

			stm = K_CreateCached(nbFaces * sizeof(XMFLOAT3) * 3);
			int currentID = 0;
			for (UINT f = 0; f<nbFaces; f++)
			{
				aiFace& face = mesh->mFaces[f];
				_ASSERT(face.mNumIndices == 3);
				stm->Write(face.mIndices, sizeof(UINT) * 3);
			}
			stm->Seek(0, eStreamOrigin::foStart);
			pMesh->SetDataIndices(stm, true);

			K_SAFERELEASE(stm);
		}
	}
}
