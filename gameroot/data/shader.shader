Texture2D diffuseTex : register(t0);
Texture2D normalTex : register(t1);
Texture2D specTex : register(t2);

SamplerState mysamplerstate;

cbuffer ConstantBuffer : register(b0)
{
	matrix world;
	matrix view;
	matrix projection;
}

struct VS_INPUT
{
	float4 position : POSITION;
	//float4 color : COLOR;
	float2 UV : TEXCOORD0;
	float3 normal : NORMAL;
	float3 binormal : BINORMAL;
	float3 tangent : TANGENT;
};

struct VS_OUTPUT
{
	float4 position : SV_POSITION;
	//float4 color : COLOR;
	float2 UV : TEXCOORD0;
	float3 normal : NORMAL;
	float3 binormal : BINORMAL;
	float3 tangent : TANGENT;
};

VS_OUTPUT VShader(VS_INPUT input)
{
	// VS_OUTPUT output;
	// output.position = input.position;
	// output.color = input.color;
	// output.UV = input.UV;
	// return output;
	VS_OUTPUT output;
	
	matrix wvp = mul(world, view);
	wvp = mul(wvp, projection);
	output.position = mul(input.position, wvp);
	//output.position = mul(output.position, view);
	//output.position = mul(output.position, projection);
	output.UV = input.UV;
	
	
	output.normal = mul(input.normal, wvp);
	output.binormal = mul(input.binormal, wvp);
	output.tangent = mul(input.tangent, wvp);
	//output.normal = mul(output.normal, view);
	//output.normal = mul(output.normal, projection);
	return output;
}
//#define PHONG
void ComputeLight(float3 normal, float3 lightdir, float3 viewdir, float3 color, 
								float intensity, float specpower,
								inout float3 light, inout float3 specular)
{
	lightdir = normalize(lightdir);
	float NdotL = saturate(dot(normal, lightdir));//saturate will clamp value from 0 to 1
	light += NdotL * color * intensity;
	
#ifdef PHONG
	float3 reflection = reflect(lightdir, normal);
	float specint = saturate(dot(reflection, viewdir));
	specint = pow(specint, specpower);
#else
	float3 H = normalize(lightdir - viewdir); //this should be light + viewdir
	float NdotH = saturate(dot(normal, H));
	float specint = pow(NdotH,specpower);
#endif
	specular += specint * color;
}


float4 PShader(VS_OUTPUT input) : SV_TARGET
{
	//float2 uv = input.UV;
	//uv.x *= 2;
	float4 diffuse = diffuseTex.Sample(mysamplerstate, input.UV);
	float3 specMap = specTex.Sample(mysamplerstate, input.UV).rgb;
	float4 normalMap = normalTex.Sample(mysamplerstate, input.UV);
	// float4 result = diffuse * spec;
	
	//Expand the range of the normal value from (0, +1) to (-1, +1).
	normalMap = normalize((normalMap * 2.0f) - 1.0f);
	
	float3x3 TBN = float3x3(input.tangent, input.binormal, input.normal);
	float3 normal = normalize(mul(normalMap, TBN));	
	//float3 normal = normalize(input.normal);	
	float3 light = float3(0,0,0);
	float3 specular = float3(0,0,0);
	float3 viewdir = float3(0,0,1);
	ComputeLight(normal, float3(0,1,0), viewdir, float3(1,1,1), 1, 500, light, specular);
	ComputeLight(normal, float3(0,0,1), viewdir, float3(1,1,1), 1, 500, light, specular);
	
	float3 finalcolor = (diffuse * light) + (diffuse * specular * specMap); 
	return float4(finalcolor, diffuse.a);
	//return result;
}